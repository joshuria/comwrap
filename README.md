# COMWrap

RAII Wrapping classes for Microsoft COM.

# TL;DR
The following classes are supported:
  - COMContext: ensuring correctly uninitializing COM resources when context variable is leaving
        its life time.
  - COMInstance: ensuring always calling `IUnknown::Release()` when COM class instance variable is
        destructed.
  - BStrWrap: RAII wrapping class of `BSTR` structure.

The following files provide helper methods:
  - logger: provides `GetLastError()` wrap and `OutputDebugString()` wrap functions.

# How to use
Just including contents under `src/josh` folder into your source.

