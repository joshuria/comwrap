#pragma once
#ifndef _JOSH_COMINSTANCE_H_
#define _JOSH_COMINSTANCE_H_

#include <comdef.h>


namespace josh {

/**RAII of COM interface instance. */
template<typename T>
class COMInstance {
public:
    /**Calling `CoCreateInstance`.
     @sa https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-cocreateinstance
    */
    COMInstance(CLSID const& clsId, GUID const& riId,
        LPUNKNOWN container = nullptr, DWORD clsContext = CLSCTX_LOCAL_SERVER);
    /**Calling `CoCreateInstance` but query ref of IID (riId) by __uuidof operator.
     @sa https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-cocreateinstance
    */
    COMInstance(CLSID const& clsId,
        LPUNKNOWN container = nullptr, DWORD clsContext = CLSCTX_LOCAL_SERVER);
    ~COMInstance() noexcept;
    COMInstance(COMInstance&&) noexcept;
    COMInstance& operator=(COMInstance&& o) noexcept;

    /**Get `CoCreateInstance` result code.
     @sa https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-cocreateinstance#return-value
     */
    [[nodiscard]]
    HRESULT getResult() const noexcept;
    /**Get instance pointer.*/
    [[nodiscard]]
    T* getPointer() noexcept;
    /**Get instance pointer.*/
    [[nodiscard]]
    T const* getPointer() const noexcept;
    /**Get reference of instance.
     Trigger SIGSEGV if instance is null (fail to create). */
    [[nodiscard]]
    T& get();
    /**Get reference of instance.
     Trigger SIGSEGV if instance is null (fail to create). */
    [[nodiscard]]
    T const& get() const;

    COMInstance(COMInstance const&) = delete;
    void operator=(COMInstance const&) = delete;

private:
    T* _instance;
    HRESULT _result;
};


/*************************************/
/*          Implementation           */
/*************************************/
template<typename T>
COMInstance<T>::COMInstance(CLSID const& clsId, GUID const& riId, LPUNKNOWN container, DWORD clsContext)
    : _instance(nullptr), _result(S_FALSE)
{
    _result = CoCreateInstance(clsId, container, clsContext, riId, reinterpret_cast<void**>(&_instance));
}


template<typename T>
COMInstance<T>::COMInstance(CLSID const& clsId, LPUNKNOWN container, DWORD clsContext)
    : _instance(nullptr), _result(S_FALSE)
{
    _result = CoCreateInstance(
        clsId, container, clsContext, __uuidof(T), reinterpret_cast<void**>(&_instance));
}


template<typename T>
COMInstance<T>::COMInstance(COMInstance<T>&& o) noexcept: _instance(o._instance), _result(o._result) {
    o._instance = nullptr;
}


template<typename  T>
COMInstance<T>& COMInstance<T>::operator=(COMInstance&& o) noexcept {
    if (_instance) _instance->Release();
    _instance = o._instance;
    o._instance = nullptr;
    _result = o._result;
    return *this;
}


template<typename T>
COMInstance<T>::~COMInstance() noexcept {
    if (!_instance) return;
    _instance->Release();
    _instance = nullptr;
}


template<typename T>
inline HRESULT COMInstance<T>::getResult() const noexcept { return _result; }


template<typename T>
inline T* COMInstance<T>::getPointer() noexcept { return _instance; }


template<typename T>
inline T const* COMInstance<T>::getPointer() const noexcept { return _instance; }


template<typename T>
inline T& COMInstance<T>::get() { return *getPointer(); }


template<typename T>
inline T const& COMInstance<T>::get() const { return *getPointer(); }

} // ! namespace josh

#endif // ! _JOSH_COMINSTANCE_H_

