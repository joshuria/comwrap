#include "BStrWrap.h"
#include <stdexcept>
#include <cstdlib>

using namespace josh;


BStrWrap::BStrWrap(std::wstring const& wstr)
    : _bstr(SysAllocStringLen(wstr.data(), static_cast<UINT>(wstr.size()))) 
{
    if (_bstr == nullptr) throw std::runtime_error( "Fail to allocate BStr");
}


BStrWrap::BStrWrap(std::string const& str): _bstr(nullptr) {
    if (str.empty()) {
        _bstr = SysAllocStringLen(L"", 0);
        return;
    }
    std::wstring wstr;
    std::size_t len;
    mbstowcs_s(&len, nullptr, 0, str.c_str(), str.size());
    wstr.resize(len);
    mbstowcs_s(nullptr, &wstr[0], wstr.size(), str.c_str(), str.size());
    _bstr = SysAllocStringLen(wstr.data(), static_cast<UINT>(wstr.size() - 1));
}


BStrWrap::BStrWrap(BStrWrap&& o) noexcept: _bstr(o._bstr) {
    o._bstr = nullptr;
}


BStrWrap::~BStrWrap() noexcept {
    SysFreeString(_bstr);
    _bstr = nullptr;
}


BStrWrap& BStrWrap::operator=(BStrWrap&& o) noexcept {
    SysFreeString(_bstr);
    _bstr = o._bstr;
    o._bstr = nullptr;
    return *this;
}

