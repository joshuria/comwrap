#pragma once
#ifndef _JOSH_COMCONTEXT_H_
#define _JOSH_COMCONTEXT_H_

#include <comdef.h>


namespace josh {

/**RAII instance for COM CoInitialize and uninitialize.
By spec of COM, instance of this class should be hold by each threads which uses COM functionality.

 @sa https://docs.microsoft.com/en-us/cpp/parallel/concrt/walkthrough-using-the-concurrency-runtime-in-a-com-enabled-application
*/
class COMContext {
public:
    /**Calling `CoInitializeEx` with specified @p param.
     @param[in] param parameter to pass to `CoInitializeEx` @b dwCoInit parameter. Default value is
        @b COINIT_MULTITHREADED.
     @sa https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-coinitializeex
     @sa https://docs.microsoft.com/en-us/windows/win32/api/objbase/ne-objbase-coinit */
    COMContext(DWORD param = COINIT_MULTITHREADED);
    ~COMContext() noexcept;

    /**Get `CoInitializeEx` result code.
     @sa https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-coinitializeex#return-value
     */
    [[nodiscard]]
    HRESULT getResult() const noexcept;

    COMContext(COMContext const&) = delete;
    COMContext(COMContext&&) noexcept = delete;
    void operator=(COMContext const&) = delete;
    void operator=(COMContext&&) noexcept = delete;

private:
    HRESULT _result;
};


/*************************************/
/*          Implementation           */
/*************************************/
inline HRESULT COMContext::getResult() const noexcept { return _result; }

} // ! namespace josh

#endif // ! _JOSH_COMCONTEXT_H_

