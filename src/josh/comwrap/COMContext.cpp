#include "COMContext.h"
#include <combaseapi.h>

using namespace josh;


COMContext::COMContext(DWORD param): _result(E_FAIL) {
    _result = CoInitializeEx(nullptr, param);
}


COMContext::~COMContext() noexcept {
    if (_result != E_FAIL) CoUninitialize();
}

