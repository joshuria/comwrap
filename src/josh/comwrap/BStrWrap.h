#pragma once
#ifndef _JOSH_BSTRWRAP_H_
#define _JOSH_BSTRWRAP_H_

#include <comdef.h>
#include <string>


namespace josh {

class BStrWrap {
public:
    explicit BStrWrap(std::wstring const& wstr);
    explicit BStrWrap(std::string const& str);
    explicit BStrWrap(BSTR bstr);
    BStrWrap(BStrWrap&& o) noexcept;
    ~BStrWrap() noexcept;
    BStrWrap& operator=(BStrWrap&& o) noexcept;
    explicit operator bool() const noexcept;
    BSTR operator*() const noexcept;

    [[nodiscard]]
    BSTR get() const;

    BStrWrap(BStrWrap const&) = delete;
    void operator=(BStrWrap const&) = delete;
private:
    BSTR _bstr;
};


/*************************************/
/*          Implementation           */
/*************************************/
inline BStrWrap::BStrWrap(BSTR bstr): _bstr(bstr) { }


inline BSTR BStrWrap::get() const { return _bstr; }


inline BStrWrap::operator bool() const noexcept { return _bstr != nullptr; }


inline BSTR BStrWrap::operator*() const noexcept { return get(); }

} // ! namespace josh

#endif // ! _JOSH_BSTRWRAP_H_

