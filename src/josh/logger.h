#pragma once
#ifndef _JOSH_LOGGER_H_
#define _JOSH_LOGGER_H_

#include <string>
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>


namespace josh {

inline void writeLog(std::string const& msg) { OutputDebugStringA(msg.c_str()); }


inline void writeLog(std::wstring const& msg) { OutputDebugStringW(msg.c_str()); }


inline void writeLogCStr(char const* msg) { return writeLog(msg); }


inline std::string writeLastError(std::string const& prefix = "") noexcept {
    auto lastError = ::GetLastError();
    if (lastError == NO_ERROR)  return "";

    char* msg;
    FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, lastError,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPSTR>(&msg),
        0, nullptr);
    try { writeLog(prefix + ": " + msg); }
    catch (...) { /* Ignore all.*/ }
    std::string str(msg);
    LocalFree(msg);
    return str;
} // ! logLastError()

} // ! namespace josh

#endif // ! _JOSH_LOGGER_H_

